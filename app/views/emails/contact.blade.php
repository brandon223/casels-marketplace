<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<strong>First Name:</strong> {{ $first }}
		<br>
		<br>
		<strong>Last Name:</strong> {{ $last }}
		<br>
		<br>
		<strong>Phone:</strong> {{ $phone }}
		<br>
		<br>
		<strong>Email:</strong> {{ $email }}
		<br>
		<br>
		<strong>Message:</strong> {{ $messageBody }}
	</body>
</html>
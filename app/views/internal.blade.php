@if ($message = Session::get('alert'))
	{{ $message }}
@endif

@include('header')
		<!--START internal page-->
			<div id="shop-now">
				<div id="page-main-title">
					<div class="wrapper">
						<h1 id="page-h1"><?php echo $pageTitle; ?></h1>
					</div>
				</div>

				<div id="content">
					<div class="wrapper">
						<div id="internal-content-main">
							<?php echo $pageContent; ?>
						</div>
						<div id="side-bar" class="shop-now-side">
							<img src="./images/sidebar.jpg" id="sidebar-img">
							<h1 id="sidebar-title">Become a Casel's Preferred Customer</h1>
							{{ HTML::link('get-card', 'Join Us', array('id' => 'sidebar-cta')) }}
							<div id="contact-hours" style="display:none;">
								<h1 style="font-size: 25px; font-family: lithosBlack;border-top: 1px solid #333;padding-top: 20px;">Store Hours:</h1>
								<p>Mon - Sat: 8:00am - 8:00pm</p>
								<p>Sunday: 8:00am - 7:00pm</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		<!--END internal page-->
@include('footer')
